#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Datenbank Exporte
# Copyright (C) 2023  Mathezirkel Augsburg

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

import os
import pandas as pd
import yaml
from pathlib import Path

with open(Path(os.getenv('MC_CONFIG_FILE')), 'r') as configFile:
    config = yaml.safe_load(configFile)

# zirkelID_separator (str): Character which separates the ZirkelID into event and Zirkel (Default: "").
zirkelID_separator =  config.get("zirkelID_separator", "")

# Date format in pandas syntax.
date_format = config["date_format"]

# Dates when the camp starts resp ends.
try:
    start_date = pd.to_datetime(config.get("start_date", None), format=date_format)
    end_date = pd.to_datetime(config.get("end_date", None), format=date_format)
except TypeError:
    print("No or invalid start or end date provided")
    start_date = None
    end_date = None
