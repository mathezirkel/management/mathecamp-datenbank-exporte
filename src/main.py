#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Datenbank Exporte
# Copyright (C) 2023  Mathezirkel Augsburg

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# warnings.filterwarnings("ignore", category=FutureWarning)
# pd.set_option("display.max.rows", None)

import os
import yaml
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import codecs
from pathlib import Path

from mc_personal_data import *


def handle_exports(df: pd.DataFrame, config: Dict, output_dir: Path) -> None:
    """
    Handles the export of various file formats from the input DataFrame based on the configuration provided.
    The export formats include csv, vcf, png and zshFile. For each export format, the function checks if the
    corresponding key exists in the configuration and if so, it proceeds with the export.

    Parameters:
        df (pd.DataFrame): The DataFrame to be exported
        config (Dict): A dictionary containing the configuration for the export process.
        output_dir (Path): Path where the outputs are saved

    Returns:
        None
    """
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for group_name, group_df in df.groupby(config.get("group_by", lambda x: "")):

        for export in config.get("csv_extract", []):
            export_df = dataframe_query(
                group_df,
                export.get("columns", []),
                export.get("condition", ""),
                export.get("sort_by", []),
                export.get("rename_columns", {})
            )
            export_df.to_csv(
                output_dir / (group_name + export["filename"]), sep=';', index=False, date_format='%d.%m.%y')

        for export in config.get("extract_list", []):
            entries = get_list_of_columns(
                group_df,
                export.get("columns", []),
                export.get("condition")
            )
            with codecs.open(output_dir / (group_name + export["filename"]), 'w', 'utf-8-sig') as txtFile:
                txtFile.write(", ".join(entries))

        for export in config.get("csv_distribution", []):
            dist_df = export_distribution(
                group_df,
                export["columns"]
            )
            dist_df.to_csv(
                output_dir / (group_name + export["filename"]), sep=';', index=True, date_format='%d.%m.%y')

        for export in config.get("csv_aggregate_by_column", []):
            aggregated_df = aggregate_items_in_column(
                group_df,
                export["column"],
                export["group_by"]
            )
            aggregated_df.to_csv(
                output_dir / (group_name + export["filename"]), sep=';', index=True, date_format='%d.%m.%y')

        for export in config.get("csv_count_occurences", []):
            export_df = count_occurences_in_column(
                group_df,
                export["column"]
            )
            export_df.to_csv(
                output_dir / (group_name + export["filename"]), sep=';', index=True, date_format='%d.%m.%y')

        for export in config.get("social_graph", []):
            export_dir = output_dir / export["dir"] / group_name
            if not os.path.exists(export_dir):
                os.makedirs(export_dir)
            wcc = get_social_graphs(group_df, export["column"], export["nodes"])
            for i, component in enumerate(wcc):
                plt.figure(figsize=(40, 20))
                pos = nx.spring_layout(component)
                nx.draw_networkx(component, pos, arrowstyle='->', arrowsize=50, node_size=500,
                                with_labels=True, font_size=12, font_weight='bold', node_color="#d6faff")
                plt.savefig(export_dir / f"wunsch_{i}.png")
                plt.clf()
                plt.close()

        for vcf_spec in config.get("vcf", []):
            list_of_vcards = create_vcf(group_df, vcf_spec)
            with codecs.open(output_dir / (group_name + vcf_spec["filename"]), 'w', 'utf-8-sig') as vcfFile:
                for card in list_of_vcards:
                    vcfFile.write(card.serialize())

        for zsh_spec in config.get("zshFile", []):
            prefix = "_participants=(\n"
            suffix = ")\n"
            export_df = group_df.copy()
            export_df["Bild"] = export_df[zsh_spec["columns_to_generate_name"]].apply(
                lambda x: make_image_filename(x), axis=1)
            # The zsh script does not work if the file is generated with codecs.open
            with open(output_dir / (group_name + zsh_spec["filename"]["zsh"]), 'w') as zshFile:
                zshFile.write(prefix)
                for _, row in export_df.iterrows():
                    entry = row["Bild"]
                    zshFile.write(f"    \'{entry}\'\n")
                zshFile.write(suffix)
            export_df = dataframe_query(
                export_df,
                zsh_spec["columns_for_card"] + ["Bild"],
                "",
                export.get("columns_to_generate_name", []),
                export.get("rename_columns", {})
            )
            export_df.to_csv(
                output_dir / (group_name + zsh_spec["filename"]["csv"]), sep=';', index=False)


def main() -> None:
    MC_LIST_DIR = Path(os.getenv("MC_LIST_DIR"))

    # Open config file
    with open(Path(os.getenv('MC_CONFIG_FILE')), 'r') as configFile:
        config = yaml.safe_load(configFile)

    for data in config["exports"]:
        source = MC_LIST_DIR / data["source"]
        output_dir = MC_LIST_DIR / data["output_dir"]

        # Read source file
        df = pd.read_csv(source, sep=';', dtype={'PLZ': str})

        # Format dataframe
        df = format_dataframe(df)

        # Handle exports
        handle_exports(df, data, output_dir)


if __name__ == "__main__":
    main()
