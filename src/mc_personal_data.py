#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Datenbank Exporte
# Copyright (C) 2023  Mathezirkel Augsburg

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import List, Dict
import numpy as np
import pandas as pd
import networkx as nx
import vobject
from config import *


def format_dataframe(df: pd.DataFrame) -> pd.DataFrame:
    """
    Transforms the input DataFrame to a format suitable for mathecamp data.
    This includes extracting first name,
    extracting Zirkel from ZirkelID, filling empty values in column 'Ernährung' with 'fleischhaltig', and filling the column 'Rechtliches Geschlecht'.

    Parameters:
        df (pd.DataFrame): The DataFrame to be transformed.

    Returns:
        pd.DataFrame: The transformed DataFrame.
    """
    format_df = df.copy()

    # Extract first name
    if "Rufname" in format_df.columns and "Vorname" in format_df.columns:
        format_df["Rufname"] = format_df.apply(lambda x: x["Vorname"].split(
            " ")[0].strip() if pd.isna(x["Rufname"]) else x["Rufname"], axis=1)

    # Extract Zirkel from ZirkelID if separator is provided
    if "ZirkelID" in format_df.columns and zirkelID_separator:
        format_df["Zirkel"] = format_df["ZirkelID"].apply(
            lambda x: x.split(zirkelID_separator)[1].strip())

    # Cast column 'Geburtsdatum' to date
    # Add a column whether a person has birthday if a start and end date is provided
    if "Geburtsdatum" in format_df.columns:
        format_df["Geburtsdatum"] = pd.to_datetime(format_df["Geburtsdatum"], format=date_format)
        if start_date and end_date:
            format_df["hat_Geburtstag"] = format_df["Geburtsdatum"].dt.strftime('%m-%d').apply(lambda x: (x >= start_date.strftime('%m-%d')) and (x <= end_date.strftime('%m-%d')) if pd.notna(x) else False)

    # Fill empty values in column 'Ernährung' with 'fleischhaltig'
    if "Ernährung" in format_df.columns:
        format_df["Ernährung"] = format_df["Ernährung"].fillna("fleischhaltig")

    # Fill empty values in column 'Rechtliches Gechlecht'
    if "Rechtliches Gechlecht" in format_df.columns and "Geschlecht" in format_df.columns:
        format_df["Rechtliches Geschlecht"] = format_df.apply(lambda x: x["Rechtliches Geschlecht"] if pd.notna(
            x["Rechtliches Geschlecht"]) else x["Geschlecht"], axis=1)

    return format_df


def dataframe_query(df: pd.DataFrame, column_list: List[str], condition: str = "", sort_by: List[str] = [], rename_columns: Dict[str, str] = {}) -> pd.DataFrame:
    """
    Exports a subset of columns and rows from a pandas DataFrame

    Parameters:
        df (pd.DataFrame): The pandas DataFrame to be exported
        column_list (List[str]): A list of column names to include in the exported DataFrame
        condition (str): A condition to filter the row interpreted by df.query() (Default: "")
        sort_by (List[str]): A list of column names by which the exported DataFrame will be sorted. (Default: [])
        rename_columns (Dict[str,str]): A dictionary where keys are the original column names and values are the new column names. This parameter allows renaming columns before export. (Default: {})

    Returns:
        pd.DataFrame: New DataFrame containing the queried columns and rows
    """

    result_df = (
        df
        .copy()
        .pipe(lambda x: x.query(condition) if condition else x)
        .sort_values(by=sort_by)
        .pipe(lambda x: x if "*" in column_list else x[column_list])
        .rename(columns=rename_columns)
    )
    return result_df


def get_list_of_columns(df: pd.DataFrame, columns: List[str], condition: str = "") -> List[str]:
    """
    Exports all unique entries in the specified columns to a list. If a cell contain comma-separated values, these are added separately.

    Parameters:
        df (pd.DataFrame): The pandas DataFrame from which entries will be extracted.
        columns (List[str]): A list of column names to include in the exported list.
        condition (str): A condition to filter the rows of the DataFrame. The condition should be a valid query string that can be interpreted by the df.query() method. (Default: "")

    Returns:
        List[str]: A list of unique entries containing all the values from the selected columns of the DataFrame. Duplicate entries are ignored.
    """
    tmp_df = (
        df
        .pipe(lambda x: x.query(condition) if condition else x)
        [columns]
        .applymap(lambda x: x.split(',\s*') if pd.notna(x) else [])
    )
    entries = list(set([entry for col in columns for entry in tmp_df[col].sum()]))
    return entries

def export_distribution(df: pd.DataFrame, cols: List[str]) -> pd.DataFrame:
    """
    Exports the distribution of values of two columns of a pandas DataFrame to a csv file.
    The exported file will contain the count of occurrences of each combination of values in the two columns,
    grouped by the values in the first column and the second column respectively, as well as the total
    count of occurrences for each group and the overall total count of occurrences.

    Parameters:
        df (pd.DataFrame): The pandas DataFrame to be exported
        cols (List[str]): List containing one or two column names to group by and count the occurrences of values.

    Returns:
        pd.DataFrame: The DataFrame containing the distribution
    """
    if len(cols) != 2:
        raise ValueError(
            f"List of columns expected to be of length 2, but got {len(cols)}")

    dist_df = (
        df
        .copy()
        .groupby(cols)
        .size()
        .reset_index(name='Anzahl')
        .pivot_table(index=cols[0], columns=cols[1], values="Anzahl", margins=True, margins_name="gesamt", aggfunc=sum)
        .astype('Int64')
    )

    return dist_df


def aggregate_items_in_column(df: pd.DataFrame, item_column: str, class_column: str) -> pd.DataFrame:
    """
    Aggregates the items in a column of a pandas DataFrame based on the values of another column.

    This function groups the DataFrame by the unique values in `class_column`, and then aggregates the values in `item_column` for each group. Specifically, the function merges the comma-separated list of items ignoring duplicates.

    Parameters:
        df (pd.DataFrame): The pandas DataFrame to be aggregated.
        item_column (str): The name of the column to be aggregated.
        class_column (str): The name of the column containing the classes to group by.

    Returns:
        pd.DataFrame: A new pandas DataFrame with the aggregated values of item_column for each class in class_column.
    """
    result_df = (
        df
        .copy()
        [[class_column, item_column]]
        .dropna(subset=[item_column])
        .assign(**{item_column: lambda x: x[item_column].str.split(',').apply(lambda y: [i.strip() for i in y])})
        .explode(item_column)
        .drop_duplicates()
        .groupby(class_column, as_index=False)
        .agg(lambda x: ', '.join(x))
        .sort_values(by=class_column)
        .reset_index(drop=True)
    )

    return result_df


def count_occurences_in_column(df: pd.DataFrame, item_column: str) -> pd.DataFrame:
    """
    Counts the occurrences of items in a column containing a comma-separated list of items.

    Parameters:
        df (pd.DataFrame): The pandas DataFrame to be processed.
        item_column (str): The name of the column containing comma-separated values to be counted.

    Returns:
        pd.DataFrame: A new DataFrame containing two columns: 'item_column' and 'Anzahl',
        where 'item_column' contains the unique items found in the original column, and 'Anzahl'
        contains the count of occurrences of each item, sorted by decreasing frequency and then
        by increasing value of 'item_column'.
    """
    result_df = (
        df
        .copy()
        [[item_column]]
        .dropna(subset=[item_column])
        .assign(**{item_column: lambda x: x[item_column].str.split(',').apply(lambda y: [i.strip() for i in y])})
        .explode(item_column)
        .groupby(item_column)
        .size()
        .reset_index(name="Anzahl")
        .sort_values(by=["Anzahl", item_column], ascending=[False, True])
        .reset_index(drop=True)
    )

    return result_df


def get_social_graphs(df: pd.DataFrame, column_name: str, nodes: List[str]) -> List[nx.DiGraph]:
    """
    This function takes a DataFrame and a column name as input. It creates a directed graph from the
    comma-separated values in the column. It then returns a list of the weakly connected components of size more than one.

    Parameters:
        df (pd.DataFrame): The input DataFrame.
        column_name (str): The name of the column containing the comma-separated values
        nodes: List of columns to concatenate for the node name

    Returns:
        List[nx.DiGraph]: A list of directed graphes describing the specified wishes.
    """
    G = nx.DiGraph()
    for _, row in df.iterrows():
        participant_node = " ".join([row[col] for col in nodes]).strip()
        G.add_node(participant_node)
        if pd.notna(row[column_name]):
            for neighbor in row[column_name].split(","):
                neighbor = neighbor.strip()
                G.add_node(neighbor)
                G.add_edge(participant_node, neighbor)
    wcc = [G.subgraph(component) for component in nx.weakly_connected_components(
        G) if len(component) > 1]
    return wcc


def create_vcf(df: pd.DataFrame, column_names: Dict) -> List[vobject.vCard]:
    """
    Create a list of vCards from a DataFrame. The columns in the DataFrame that should be used for each vCard attribute
    (e.g. name, organization, email, phone number) should be specified in a dictionary called column_names.

    Parameters:
        df (pd.DataFrame): The DataFrame containing the data to be used for the vCards.
        column_names (Dict): A dictionary containing the names of the columns in the DataFrame that should be used for each vCard attribute.

    Returns:
        List[vobject.vCard]: A list of vCard objects created from the data in the DataFrame.
    """
    def create_card(row):
        card = vobject.vCard()

        # Name
        n = card.add("N")
        n.value = vobject.vcard.Name(
            family=row.get(column_names["name"]["family"], "").strip(),
            given=row.get(column_names["name"]["given"], "").strip()
        )

        # Formatted Name (required)
        fn = card.add("FN")
        formatted_name = row.get(column_names["name"]["given"], "").strip(
        ) + " " + row.get(column_names["name"]["family"], "").strip()
        fn.value = formatted_name

        # Address
        if column_names.get("address"):
            adr = card.add("ADR")
            adr.value = vobject.vcard.Address(
                code=str(row.get(column_names["address"]["zip"], "")),
                city=row.get(column_names["address"]["city"], ""),
                street=row.get(column_names["address"]["street"], "")
            )

        # Org
        if column_names.get("org"):
            org = card.add("ORG")
            org.value = [column_names["org"] + " " +
                         str(row.get(column_names["org"], ""))]

        # E-Mails
        list_of_emails = []
        for email_field in column_names["email"]:
            if pd.notna(row.get(email_field, np.nan)):
                list_of_emails += row[email_field].split(',')

        for email in list_of_emails:
            card.add("EMAIL").value = email.strip()

        # Telefon
        for telefon_type in column_names["telefon"]:
            list_of_numbers = []
            for telefon_field in column_names["telefon"][telefon_type]:
                if pd.notna(row.get(telefon_field, np.nan)):
                    list_of_numbers += row[telefon_field].split(',')

            for number in list_of_numbers:
                tel = card.add("TEL")
                tel.value = number.strip()
                tel.type_param = telefon_type.upper()

        return card

    list_of_vcards = df.apply(create_card, axis=1).tolist()

    return list_of_vcards


def make_image_filename(series: pd.Series) -> str:
    """
    Concatenates the given strings, replaces special characters by their ASCII counterparts
    and removes whitespaces.

    Parameters:
        series (pd.Series): Series of the strings to concatenate

    Returns:
        str: The concatenated string
    """
    special_char_map = {ord('ä'): 'ae', ord('ü'): 'ue',
                        ord('ö'): 'oe', ord('ß'): 'ss'}
    result = (
        "".join(series)
        .translate(special_char_map)
        .replace(" ", "")
        .lower()
    )
    return result
