# Mathecamp Datenbank Exporte

DEPRECATED: Nach der Migration auf die Mathezirkel App ist dieses Repository obsolet.
Notwendige Funktionalität wurde [hierhin](https://gitlab.com/mathezirkel/management/organization-helper) übernommen.

## Beschreibung

Das Skript verarbeitet den Datenbankauszug für das Mathecamp und erstellt daraus notwendige Dateien für die Organisation.

## Anleitung

### Datenbankauszug erstellen

Erstelle einen Auszug aus der Datenbank und lege ein Spreadsheet für die Betreuer:innen an.

Beispiele für die Felder sind im Ordner [example](./example/) zu finden.

### Konfigurationsdatei anpassen

Passe die Konfigurationsdatei entsprechend der gewünschten Outputs an.

Ein Beispiel ist im Ordner [example](./example/) zu finden.

### CSV-Datei erstellen

```commandline
$ libreoffice --convert-to csv:"Text - txt - csv (StarCalc)":59,0,76,1,1 \
    --outdir CHANGEME \
    CHANGEME.ods
```

Erklärung der Parameter:
* 59: String delimiter ;
* 0: Text delimiter empty
* 76: Encoding UTF-8
* 1: Line number to start reading
* 1: Cell Format Standard

### Skript ausführen

```commandline
$ export MC_LIST_DIR=path/to/lists
$ export MC_CONFIG_FILE=path/to/config/file
$ export TAG=CHANGME
$ docker run \
    -v $MC_LIST_DIR:/app/lists \
    -v $MC_CONFIG_FILE:/app/config.yaml \
    registry.gitlab.com/mathezirkel/management/datenbank-exporte:$TAG
```

## Dokumentation

### Umgebungsvariablen

* MC_LIST_DIR: Pfad des Ordners, in dem die Ausgabedateien gespeichert werden sollen.
* MC_CONFIG_FILE: Pfad der Konfigurationsdatei

Im Dockerfile werden die Umgebungsvariablen statisch gesetzt und als Volumes übergeben.

### Modifizierung der Quelldatei

Beim Einlesen werden die folgenden Spalten, falls vorhanden, im Arbeitsspeicher angepasst bzw. ergänzt:

| Spaltenname | Beschreibung | Bedingung |
|-------------|--------------|-----------|
| `Rufname` | Leere Einträge werden durch den ersten Vornamen ersetzt | |
| `ZirkelID` &rarr; `Zirkel` | Extrahiert den Zirkel aus der ZirkelID | Parameter `zirkelID_separator` |
| `Ernährung` | Leere Einträge werden durch `fleischhaltig` aufgefüllt | |
| `Rechtliches Geschlecht` | Leere Einträge werden durch den Eintrag in Spalte `Geschlecht` ersetzt. | |
| `hat_Geburtstag` | Boolean, ob entsprechende Person während des Camps Geburtstag hat. | Parameter `start_date` und `end_date`|

### Konfigurationsdatei

#### Allgemeine Parameter

* zirkelID_separator (optional): Separator in den ZirkelIDs. ZirkelIDs haben die Form `zirkeljahrSeparatorZirkel`, z.B. `2223C5a`.
  Falls vorhanden, wird die Spalte `Zirkel` hinzugefügt.
* date_format (required): Datumsformat in den Quelldateien, siehe [hier](https://docs.python.org/3/library/datetime.html#strftime-and-strptime-behavior).
* start_date und end_date (optional): Start- bzw. Enddatum der Veranstaltung im Format `date_format`.
  Falls vorhanden, wird die Spalte `hat_Geburtstag` hinzugefügt.

#### Exports

Pro Datensatz wird ein Item angelegt, das das folgende Dictionary enthält:

* `source` (required): Quelldatei (csv) relativ zu MC_LIST_DIR
* `output_dir` (required): Verzeichnis zur Ausgabe der Dateien relativ zu MC_LIST_DIR
* `group_by` (optional): Pro Gruppe in der angegebenen Spalte wird eine separate Datei erstellt. Der Dateiname setzt sich aus dem Name der Gruppe und dem angegebenen `filename` zusammen.
* `csv_extract` (optional): Export von Zeilen und Spalten nach den folgenden Kriterien

    | Wert | Datentyp | Beschreibung |
    |------|----------|--------------|
    | `condition` (optional) | str | Bedingung, die die exportierten Zeilen erfüllen muss (Style: pandas.DataFrame.query()) |
    | `columns` (required) | List[str] | Spalten, welche exportiert werden (Wenn `*` enthalten ist, werden alle Spalten exportiert.) |
    | `sort_by` (optional) | List[str] | Spalten, nach denen alphanumerisch sortiert wird (in absteigender Priorität) |
    | `rename_columns` (optional) | Dict[str, str] | Umbenennung von Spalten in der Ausgabedatei (Format: "Name_alt : Name_neu") |
    | `filename` (required) | str | Dateiname der exportierten Datei |

* `extract_list` (optional): Export der Werte von Zellen in bestimmten Zeilen und Spalten als komma-separierte Liste. Komma-separierte Werte in einer Zelle werden dabei als separate Werte interpretiert.

    | Wert | Datentyp | Beschreibung |
    |------|----------|--------------|
    | `condition` (optional) | str | Bedingung, die die exportierten Zeilen erfüllen muss (Style: pandas.DataFrame.query()) |
    | `columns` (required) | List[str] | Spalten, welche exportiert werden. |
    | `filename` (required) | str | Dateiname der exportierten Datei |

* `csv_distribution` (optional): Erstellung einer Tabelle mit den verschiedenen Werten einer Spalte bzw. den Wertekombinationen zweier Spalten sowie deren Anzahl.

    | Wert | Datentyp | Beschreibung |
    |------|----------|--------------|
    | `columns` (required) | List[str] | Liste von Spalten (1 oder 2) |
    | `filename` (required) | str | Dateiname der exportierten Datei |

* `csv_aggregate_by_column` (optional): Ausgabe einer Liste komma-separierter Werte ohne Duplikate, die nach einer Spalte gruppiert sind. Die Werte stammen dabei aus einer Spalte, die mehrere durch Komma separierte Werte enthalten kann.

    | Wert | Datentyp | Beschreibung |
    |------|----------|--------------|
    | `column` (required) | str| Spalte mit den Werten |
    | `group_by` (required) | List[str] | Spalte, nach der gruppiert wird |
    | `filename` (required) | str | Dateiname der exportierten Datei |

* `csv_count_occurences` (optional): Zählt die absolute Häufigkeit von Werten aus einer Spalte. Die entsprechende Spalte darf dabei mehrere durch komma getrennte Werte enthalten.

    | Wert | Datentyp | Beschreibung |
    |------|----------|--------------|
    | `column` (required) | str | Spalte mit den Werten |
    | `filename` (required) | str | Dateiname der exportierten Datei |

* `social_graph` (optional): Erstellt einen Graph, der anzeigt, wer wen angegeben hat. Die Knoten sind die Teilnehmer:innen. Eine Kante zeigt von A nach B, falls A in dem entsprechenden Feld B angegeben hat.

    | Wert | Datentyp | Beschreibung |
    |------|----------|--------------|
    | `nodes` (required) | List[str] | Liste an Spalten, deren Einträge mit " " konkateniert und als Knotennamen verwendet werden |
    | `column` (required) | List[str] | Spalte, die eine komma-separierte Liste der angegebenen Personen enthält |
    | `dir` (required) | str | Name des Ordners, in dem die Bilder gespeichert werden. |

* `vcf` (optional): Erstellung einer Kontaktdatei im Format vcf

    | Wert | Datentyp | Beschreibung |
    |------|----------|--------------|
    | `name.family` (required) | str | Spalten mit Nachnamen |
    | `name.given` (required) | str | Spalte mit Vorname |
    | `email` (required) | List[str] | Liste an Spalten mit E-Mails (Die Spalten dürfen mehrere E-Mailadressen durch Komma getrennt enthalten) |
    | `address.street` (optional) | str | Spalte mit Straße |
    | `address.zip` (optional) | str | Spalte mit PLZ |
    | `address.city` (optional) | str | Spalte mit Straße |
    | `telefon.cell` (required) | List[str] | Liste an Spalten mit Telefonnummern (Mobil) (Die Spalten dürfen mehrere E-Mailadressen durch Komma getrennt enthalten) |
    | `telefon.home` (required) | List[str] | Liste an Spalten mit Telefonnummern (Festnetz) (Die Spalten dürfen mehrere E-Mailadressen durch Komma getrennt enthalten) |
    | `telefon.work` (required) | List[str] | Liste an Spalten mit Telefonnummern (Arbeit) (Die Spalten dürfen mehrere E-Mailadressen durch Komma getrennt enthalten) |
    | `address.org` (optional) | str | Spalte mit Organisation (Format: `Spaltenname Wert`) |
    | `filename` (required) | str | Dateiname der exportierten Datei |

* `zshFile` (optional): Erstellt die notwendigen Dateien für das [Skript zur Erstellung der Kinderkarteikarten](https://gitlab.com/mathezirkel/management/ankunft).
  * zsh-Datei für die Autocompletion
  * csv inkl. Dateinamen der Bilder

  | Wert | Datentyp | Beschreibung |
  |------|----------|--------------|
  | `columns_to_generate_name` (required) | List[str] | Spalten, um den Dateinamen der Bilder zu generieren |
  | `filename.zsh` (required) | str | Dateiname der exportierten zsh-Datei |
  | `columns_for_card` (required) | List[str] | Spalten, die auf den Karteikarten verwendet werden |
  | `rename_columns` (optional) | Dict[str, str] | Umbenennung von Spalten in der Ausgabedatei (Format: "Name_alt : Name_neu") |
  | `filename.csv` (required) | str | Dateiname der exportierten csv.-Datei |

## Autor:innen
An der Entwicklung des Skripts waren Kathrin Helmsauer, Anna Rubeck, Sven Prüfer und Felix Stärk beteiligt.

## License
This project is licenced unter GPL-3.0, see [LICENSE](./LICENSE).
