FROM python:3.11.2-buster

# Set working directory
WORKDIR /app

# Copy requirements.txt into image and install dependencies
COPY requirements.txt /app
RUN pip install --no-cache-dir -r requirements.txt

# Define volumes
VOLUME /app/lists

# Copy source files into image
COPY src /app/src

# Set non-root user
RUN useradd -ms /bin/bash user
RUN chown -R user /app
USER user

# Set environment variables
ENV MC_LIST_DIR=/app/lists
ENV MC_CONFIG_FILE=/app/config.yaml

# Set entrypoint
CMD ["python", "src/main.py"]
